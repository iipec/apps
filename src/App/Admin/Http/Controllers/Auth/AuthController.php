<?php namespace Admin\Http\Controllers\Auth;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Admin\Http\Requests\Auth\AuthRequest;
use Admin\Http\Repositories\Auth\AuthRepository;

class AuthController extends Controller {
	
	protected $auth;

	public function __construct(AuthRepository $auth) {
		$this->auth = $auth;
	}

	public function index() {
		return $this->render("login");
	}

	public function auth(AuthRequest $request) {
		return $this->auth->attempt($request, "admin");		
	}

	public function logout() {
		return $this->auth->logout("admin");		
	}

}

/* End of Controller AuthController.php */