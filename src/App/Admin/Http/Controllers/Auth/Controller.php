<?php namespace Admin\Http\Controllers\Auth;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Admin\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController {

	protected $path = "admin::app.auth.";
	
}

/* End of Controller Controller.php */