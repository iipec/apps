<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

Route::get("/", function() {

	return "AdminWeb";

});

Route::get("/auth", "Auth\AuthController@index");
Route::post("/auth", "Auth\AuthController@auth");

Route::get("/logout", "Auth\AuthController@logout");

/* End of helper route.php */