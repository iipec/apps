@extends("admin::layouts.rose.master")

@section("css")
@parent<link href="{{url('apps/packages/admin/css/auth.css')}}" rel="stylesheet">
@endsection

	@section("title")
	{{__("admin::auth.title")}}
	@endsection

	@section("body")
		
		<section class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			
			@section("content")
			
			<article class="auth">

				{!! Form::open(["url" => "admin/auth", "method" => "POST"]) !!}

					{!! Alert::prefix("auth")->fire("alert::article") !!}

					<section class="form-group">

						{!! $errors->first("usr", '<p class="error"> <i class="mdi mdi-secutiry"></i> :message </p>') !!}
						
						<article class="input-group">							
							
							<span class="input-group-addon">
								<i class="mdi mdi-lock"></i>
							</span>

							{!! Form::text("usr", old("usr"), [
								"class"		=> "form-control",
								"placeholder"	=> __("admin::auth.form.place-usr")
							]) !!}

						</article>

					</section>

					<section class="form-group">

						{!! $errors->first("pwd", '<p class="error"> <i class="mdi mdi-secutiry"></i> :message </p>') !!}
						
						<article class="input-group">							
							
							<span class="input-group-addon">
								<i class="mdi mdi-key"></i>
							</span>

							{!! Form::password("pwd", [
								"class"			=> "form-control",
								"placeholder"	=> __("admin::auth.form.place-pwd")
							]) !!}
							
						</article>

					</section>

					<section class="form-group">
						
						{!! Form::button('<i class="mdi mdi-login"></i> '.__("admin::auth.form.btn"), [
							"class"		=> "btn btn-primary btn-sm btn-right",
							"type"		=> "submit"
						]) !!}

					</section>

				{!! Form::close() !!}

				<footer class="auth-footer">
					<article class="brand">
						{!! __("admin::auth.brand") !!}
					</article>
				</footer>

			</article>

			@show

		</section>

	@endsection