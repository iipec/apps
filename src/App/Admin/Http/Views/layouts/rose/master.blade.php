<!DOCTYPE html>

<html lang="{{$language}}">

	<head>

		<meta charset="{{$charset}}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="{{url('apps/packages/admin/images/icon.png')}}" rel='shortcut icon' type='image/png'/>

		<!-- CSRF Token -->
    	<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield("title", "Apps V-0.1")</title>

		@section("css")
		<link href="{{url('apps/packages/admin/css/bootstrap.min.css')}}" rel="stylesheet">
	    <link href="{{url('apps/packages/admin/css/mdi-2.1.19.min.css')}}" rel="stylesheet">
	    <link href="{{url('apps/packages/admin/css/set.css')}}" rel="stylesheet">
	    <link href="{{url('apps/packages/admin/css/layout.css')}}" rel="stylesheet">
		@show

	</head>

	<body>

		<article class="container-fluid">

			@yield("body", 'Body Page')
			
		</article>
				
		@section("js")
		<script src="{{url('apps/packages/admin/js/jquery-3.2.1.min.js')}}"></script>
	    <script src="{{url('apps/packages/admin/js/bootstrap.min.js')}}"></script>
	    <script src="{{url('apps/packages/admin/js/layout.js')}}"></script>
		@show		
	</body>

</html>