<?php namespace Admin\Http\Middleware;

/**
*---------------------------------------------------------
* ©IIPEC
*---------------------------------------------------------
*/

use Apps\Accessors\MiddlewareAccessor;

class LoadMiddleware extends MiddlewareAccessor {
	
    protected $middleware = [
    ];

    protected $middlewareGroups = [

      "admin"  => [
        
      	\App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Admin\Http\Middleware\AdminMiddleware::class,
        \Admin\Http\Middleware\Auth\AuthMiddleware::class,

      ],


    ];

    protected $routeMiddleware = [
    ];

}

/* End of LoadMiddleware.php */