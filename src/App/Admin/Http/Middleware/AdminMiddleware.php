<?php namespace Admin\Http\Middleware;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware {

    public function handle($request, Closure $next, $guard = "admin") {

    	view()->share([
    		"language"	=> config("language"),
    		"charset"	=> config("charset")
    	]);

        return $next($request);
    }

}

/* End of Library AdminMiddleware.php */