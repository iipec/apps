<?php namespace Admin\Http\Middleware\Auth;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthMiddleware {
	
	protected $exerts = [
		"admin/auth","admin/logout"
	];

    public function handle($request, Closure $next, $guard = "admin") {
    	
    	if(auth()->guard($guard)->guest() && !in_array($request->path(), $this->exerts)) {
    		
    		return redirect("admin/auth")->with([
    			"auth_info" => __("admin::auth.messages.info-auth")
    		]);	

    	}

        return $next($request);
    }

}

/* End of Library AuthMiddleware.php */