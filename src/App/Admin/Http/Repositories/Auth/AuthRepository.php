<?php namespace Admin\Http\Repositories\Auth;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


class AuthRepository {
	
	protected $user;

	public function __construct() {
		$this->user = user("userdb");
	}

	public function attempt($request, $guard) {

		if( !empty($user = $this->user->src($request->get("usr"))) ) {

			$email 		= $user->email;
			$password 	= $request->get("pwd");

			if($user->activated < 4) {

				if(auth()->guard($guard)->attempt(["email" => $email, "password" => $password])) {

					if($user->activated == 0) {
						auth()->guard($guard)->logout();
						return back()->with("auth_warning", __("admin::auth.messages.inactive-auth"));
					}

					if($user->activated == 2) {
						auth()->guard($guard)->logout();
						return back()->with("auth_warning", __("admin::auth.messages.suspend-auth"));
					}

					if($user->activated == 3) {
						auth()->guard($guard)->logout();
						return back()->with("auth_warning", __("admin::auth.messages.block-auth"));
					}

					if($user->activated == 4) {
						auth()->guard($guard)->logout();
						return back()->with("auth_error", __("admin::auth.messages.legal-auth"));
					}
					
					return redirect()->intended('admin');

				}
			}
		}

		return back()->withInput()->with("auth_error", __("admin::auth.messages.bad-auth"));

	}

	public function logout($guard) {

		auth()->guard($guard)->logout();
		return redirect("admin/auth")->with("auth_success", __("admin::auth.messages.logout-auth"));
		
	}

}

/* End of Controller AuthRepository.php */