<?php namespace Admin\Http\Requests\Auth;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest {
	
	public function authorize() {

        return true;

    }

    public function rules() {

        return [
            "usr"	=> "required",
            "pwd"	=> "required",
        ];

    }

    public function messages() {

        return [
            "required"  => __("admin::auth.validator.required"),
        ];

    }

}

/* End of AuthRequest.php */