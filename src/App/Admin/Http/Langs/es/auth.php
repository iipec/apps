<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

return [

	"title"	=> "Autenticación",

	"brand"	=> '<strong>&COPY;IIPEC</strong> Santo Domingo República Dominicana.',

	"form"	=> [

		"place-usr" => "Usuario o Correo Electronico",
		"place-pwd"	=> "Contraseña de acceso",
		"btn"		=> "Acceder",
	],

	"messages"			=> [
		"logout-auth"	=> "Cuenta cerrada correctamente.",
		"info-auth"		=> "Debe identificarse para poder ingresar a su cuenta.",

		"suspend-auth"	=> [
			0 => "Cuenta suspendida temporalmente.",
			1 => "Pongase en contacto con soporte",
		],

		"block-auth"	=> "Cuenta bloqueada.",
		"legal-auth"	=> "Su cuenta fue enviada a legales.",
		"bad-auth"		=> "Credenciales incorrectas.",
	],

	"validator"	=> [
		"required"	=> "Campo requerido",
	],

];

/* End of helper auth.php */