<?php namespace Admin;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\ServiceProvider;

class Kernel {

	public function providers() {

		return [

			\Admin\System\Providers\AdminServiceProvider::class,
			\Admin\Http\Middleware\LoadMiddleware::class,

			\Admin\System\Providers\RouteServiceProvider::class,

		];
		
	}

	public function alias() {

		return [

		];
		
	}

	public function handler($app) {
		
	}

}


/* End of providers Kernel.php */