<?php namespace Admin\System\Providers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {
    
    public function boot() {
        parent::boot();
    }
    
    public function map() {

        Route::prefix("admin")
        	   ->middleware('admin')
        	   ->namespace("Admin\Http\Controllers")
        	   ->group(__path("App/Admin/Http/Routes/route.php"));

    }

}


/* End of providers RouteServiceProvider.php */