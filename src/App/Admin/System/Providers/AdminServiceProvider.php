<?php namespace Admin\System\Providers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider {

	public function boot() {

		$this->loadTranslationsFrom(__path("App/Admin/Http/Langs"), 'admin');
		$this->loadViewsFrom(__path("App/Admin/Http/Views"), 'admin');

		$this->publishes([
	        __path("Storage/Public/Apps/packages/admin") => public_path('apps/packages/admin'),
	    ], 'admin');

	}

	public function register() {

	}

}


/* End of providers AdminServiceProvider.php */