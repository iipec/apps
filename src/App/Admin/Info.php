<?php namespace Admin;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

class Info {

	public function apps() {

		return [

			"type"			=> "package",
			"slug"			=> "admin",
			"kernel"		=> \Admin\Kernel::class,
			"token"			=> NULL,
			"activated" 	=> 1,

		];

	}

	public function meta() {

		return [
			
			"name"			=> "Admin",
			"author"		=> "Ing. Ramón A Linares Febles",
			"email"			=> "rlinares4381@gmail.com",
			"license"		=> "MIT",
			"site"			=> "http://www.iipec.net",
			"version"		=> "V-1.0",
			"description" 	=> "Administración del Hypervisor.",

		];

	}

	public function handler($apps) {
		$apps->create($this->apps())->addMeta($this->meta());
	}

}

/* End of Info.php */