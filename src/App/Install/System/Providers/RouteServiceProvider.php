<?php namespace Install\System\Providers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

    
    public function boot() {
        parent::boot();
    }

    
    public function map() {
    	
        Route::prefix("install")
    		->middleware('install')
            ->namespace("Install\Http\Controllers")
            ->group(__path('App/Install/Http/routes.php'));

    }

    public function forge() {

    	

    }

}


/* End of providers RouteServiceProvider.php */