<?php namespace Install\System\Providers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\ServiceProvider;

class InstallServiceProvider extends ServiceProvider {

	public function boot() {

		$this->loadTranslationsFrom(__path("App/Install/Http/Langs"), 'install');
		$this->loadViewsFrom(__path("App/Install/Http/Views"), 'install');

	}

	public function register() {

	}

}


/* End of providers InstallServiceProvider.php */