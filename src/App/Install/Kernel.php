<?php namespace Install;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\ServiceProvider;

class Kernel {

	public function providers() {

		return [
			\Library\Alerts\AlertServiceProvider::class,
			\Library\Users\UserServiceProvider::class,
			\Library\Groups\GroupServiceProvider::class,

			\Install\Http\Middleware\Load::class,

			\Install\System\Providers\InstallServiceProvider::class,
			\Install\System\Providers\RouteServiceProvider::class,
		];
		
	}

	public function alias() {

		return [
			"Alert"	=> \Library\Alerts\Support\Alert::class,
			"Group" => \Library\Groups\Support\Group::class,
			"User" 	=> \Library\Users\User::class,
		];
		
	}

	public function handler($app) {

		
	
	}

}


/* End of providers Kernel.php */