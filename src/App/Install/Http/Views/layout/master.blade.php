<!DOCTYPE html>

<html lang="{{$language}}">

	<head>

		<meta charset="{{$charset}}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="{{url('apps/packages/install/images/icon.png')}}" rel='shortcut icon' type='image/png'/>

		<!-- CSRF Token -->
    	<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield("title", "Apps V-0.1")</title>

		@section("css")
		<link href="{{url('apps/packages/install/css/bootstrap.min.css')}}" rel="stylesheet">
	    <link href="{{url('apps/packages/install/css/mdi-2.1.19.min.css')}}" rel="stylesheet">
	    <link href="{{url('apps/packages/install/css/layout.css')}}" rel="stylesheet">
		@show

	</head>

	<body>

		<article class="container-fluid">

			<section class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
				
				@yield("body", 'Content Page')

			</section>
			
		</article>
				
		@section("js")
		<script src="{{url('apps/packages/install/js/jquery-3.2.1.min.js')}}"></script>
	    <script src="{{url('apps/packages/install/js/bootstrap.min.js')}}"></script>
	    <script src="{{url('apps/packages/install/js/layout.js')}}"></script>
		@show		
	</body>

</html>