@extends("install::layout.master")

@section("title")
{{__("install::words.install")}}
@endsection

	@section("body")

	<article class="panel panel-default">
		
		<section class="panel-body">

			<h4>{!! $title !!}</h4>

			{!! Alert::prefix("system")->fire() !!}
			
			<p>{!! $info !!}</p>		

			@yield("content", "Content Page")
			
		</section>

		<footer class="panel-footer">

			<div class="copyright">
				{!! __("install::args.brand") !!}
			</div>

		</footer>

	</article>

	@endsection