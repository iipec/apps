@extends("install::layout.panel")

	@section("content")

		{!! Form::open(["url" => "install/account", "method" => "POST"]) !!}

			<article class="form-group">

				{!! $errors->first("pass", '<p class="error"> <i class="mdi mdi-security"></i> :message </p>') !!}
				
				{!! Form::password("pass", [
					"class"			=> "form-control",
					"placeholder"	=> __("install::args.account.form.place-pass")
				]) !!}

			</article>

			<article class="form-group">

				{!! $errors->first("repass", '<p class="error"> <i class="mdi mdi-security"></i> :message </p>') !!}
				
				{!! Form::password("repass", [
					"class"			=> "form-control",
					"placeholder"	=> __("install::args.account.form.place-repass")
				]) !!}

			</article>

			<article class="form-group">
				
				<div class="btn-group">
					
					<a href="{{url('install/env')}}" class="btn btn-primary">
						<i class="mdi mdi-chevron-double-left"></i>
						{{__("install::words.return")}}
					</a>

					{!! Form::button('<i class="mdi mdi-content-save"></i> '.__("install::words.save"), [
						"class"	=> "btn btn-primary",
						"type" => "submit"
					]) !!}

				</div>

			</article>

		{!! Form::close() !!}

	@endsection