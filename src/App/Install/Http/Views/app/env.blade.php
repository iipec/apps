@extends("install::layout.panel")

	@section("content")

		{!! Form::open(["url" => "install/env", "method" => "POST"]) !!}

			<article class="form-group">
				
				{!! Form::textarea("env", $env, [
					"class"			=> "form-control",
					"spellcheck"	=> "false",
				]) !!}

			</article>

			<article class="form-group">
				
				<div class="btn-group">
					
					<a href="{{url('install')}}" class="btn btn-primary">
						<i class="mdi mdi-chevron-double-left"></i>
						{{__("install::words.return")}}
					</a>

					{!! Form::button('<i class="mdi mdi-reload"></i> '.__("install::words.update"), [
						"class"	=> "btn btn-primary",
						"type" => "submit"
					]) !!}

					<a href="{{url('install/database-create')}}" class="btn btn-primary">
						<i class="mdi mdi-database"></i>
						{{__("install::words.entity-create")}}
					</a>

				</div>

			</article>

		{!! Form::close() !!}

	@endsection