@extends("install::layout.panel")

	@section("content")
			
		<article class="alert alert-warning">

			<strong>Warning : </strong> {{__("install::args.finished.warning")}}
			
		</article>

		<article class="btn-group">
					
			<a href="{{url('install')}}" class="btn btn-primary">
				<i class="mdi mdi-chevron-double-left"></i>
				{{__("install::words.return")}}
			</a>

			<a href="{{url('install/out')}}" class="btn btn-primary">
				<i class="mdi mdi-logout"></i>
				{{__("install::words.out")}}
			</a>

			<a href="{{url('install/go-to-admin')}}" class="btn btn-primary">
				<i class="mdi mdi-worker"></i>
				{{__("install::words.go-to-admin")}}
			</a>

		</article>

	@endsection