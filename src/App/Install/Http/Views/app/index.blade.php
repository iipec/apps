@extends("install::layout.panel")
	
	@section("css")
	<link href="{{url('css/app.css')}}" rel="stylesheet">
	<style rel="stylesheet">
		body{
			background: #fff;
			margin-top: 12%;
			color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
		}
		.panel-body{
			text-align: center;
		}

		.panel-body h4 {
			font-size: 34px;
		}

		.panel-body .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
        .panel-footer .copyright {
        	color: #996;
        	font-size: 12px;        	
        }


	</style>
	@endsection

	@section("content")

		<div class="links">
			<a href="{{url('install/published')}}">{{__("install::words.install")}}</a>
			<a href="#">GitHub</a>
			<a href="#">{{__("install::words.documentation")}}</a>
			<a href="#">{{__("install::words.news")}}</a>
		</div>

	@endsection

	@section("js")
	<script src="{{url('js/app.js')}}"></script>
	@endsection