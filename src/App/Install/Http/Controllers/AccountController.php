<?php namespace Install\Http\Controllers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Install\Http\Requests\PasswordRequest;
use Install\Http\Repositories\AccountRepository;

class AccountController extends Controller {
	
	protected $user;

	public function __construct(AccountRepository $user) {
		$this->user = $user;
	}

	public function index() {
		
		return $this->render("account", [
			"title"	=> '<i class="mdi mdi-account-circle"></i> '.__("install::args.account.title"),
			"info"	=> __("install::args.account.info"),
		]);

	}

	public function updatePassword(PasswordRequest $request) {
		return $this->user->updatePassword($request);
	}

}

/* End of Controller AccountController.php */