<?php namespace Install\Http\Controllers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Install\Http\Requests\EnvRequest;
use Install\Http\Repositories\EnvRepository;

class EnvController extends Controller {

	protected $env;
	
	public function __construct(EnvRepository $env) {

		$this->env = $env;
	
	}

	public function index() {

		return $this->render("env", [
			"env"	=> $this->env->get(),
			"title"	=> '<i class="mdi mdi-server"></i> '.__("install::args.env.title"),
			"info"	=> __("install::args.env.info")
		]);

	}

	public function update(EnvRequest $request) {
		
		return $this->env->update($request);

	}

}

/* End of Controller EnvController.php */