<?php namespace Install\Http\Controllers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController {

	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected $path = "install::app.";

	public function render($view=NULL, $data=[], $mergeData=[]) {

    	
    	if(view()->exists(($path = $this->path.$view))) {
    		
    		return view($path, $data, $mergeData);
    	}

    	abort(500, "La vista {$path} no existe");

    }

}

/* End of Controller Controller.php */