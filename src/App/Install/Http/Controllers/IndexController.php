<?php namespace Install\Http\Controllers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


class IndexController extends Controller {
	
	public function __construct() {
	}

	public function index() {
		
		return $this->render("index", [
			"title"	=> __("install::words.hypervisor")." V-1.0",
			"info"	=> __("install::args.index.info"),
		]);
		
	}

	public function published() {

		\Artisan::call("vendor:publish", ["--tag" => "apps", "--force" => true]);

		return redirect()->to("install/env")->with("system_success", __("install::args.index.message.success"));

	}

}

/* End of Controller IndexController.php */