<?php namespace Install\Http\Controllers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Install\Http\Repositories\FinishedRepository;

class FinishedController extends Controller {

	protected $end;
	
	public function __construct(FinishedRepository $end) {
		$this->end = $end;
	}

	public function index() {
		
		return $this->render("finished", [
			"title"	=> '<i class="mdi mdi-check-all"></i> '.__("install::args.finished.title"),
			"info"	=>  __("install::args.finished.info"),
		]);
		
	}

	public function outInstall() {
		return $this->end->out('/');
	}

	public function goToAdmin() {
		return $this->end->goToAdmin('/admin');
	}

}

/* End of Controller FinishedController.php */