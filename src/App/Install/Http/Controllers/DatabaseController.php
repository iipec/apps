<?php namespace Install\Http\Controllers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Install\Http\Repositories\DatabaseRepository;

class DatabaseController extends Controller {
	
	protected $db;

	public function __construct(DatabaseRepository $db) {
		
		$this->db = $db;
		
	}

	public function index() {
		
		return $this->db->migration();
		
	}

}

/* End of Controller DatabaseController.php */