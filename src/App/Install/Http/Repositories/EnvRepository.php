<?php namespace Install\Http\Repositories;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


use Illuminate\Filesystem\Filesystem;

class EnvRepository {
	
	protected $file;

	public function __construct(Filesystem $file) {
		
		$this->file = $file;

	}

	public function get() {

		return $this->file->get(base_path('.env'));

	}

	public function update($request) {
		
		$this->file->put(base_path(".env"), $request->get("env"));
		return back()->with("system_success", __("install::args.env.message.success"));

	}

}

/* End of Controller EnvRepository.php */