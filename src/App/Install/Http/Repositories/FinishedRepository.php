<?php namespace Install\Http\Repositories;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


class FinishedRepository {
	
	public function __construct() {
	
	}

	public function toggle($url=NULL) {

		if( ($apps = core("apps")->src("core", "hypervisor"))->count() > 0 ) {

			$apps->activated = 1;

			if($apps->save()) {
				return redirect()->to($url);
			}
		}

		return back();

	}

	public function out($url) {
		return $this->toggle($url);
	}

	public function goToAdmin($url) {
		return $this->toggle($url);
	}

}

/* End of Controller FinishedRepository.php */