<?php namespace Install\Http\Repositories;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


class DatabaseRepository {
	
	public function __construct() {
	
	}

	public function migration() {


		## Delete if exists local entities
		(new \CreateConfigSchema)->down();
		(new \CreateAppsMetaSchema)->down();
		(new \CreateAppsSchema)->down();

		if(\Schema::hasTable("migrations")) {
			\Artisan::call("migrate:reset", ["--path" => __MIGRATE__]);
			\Schema::dropIfExists('migrations');
		}

		## Create local entities
		(new \CreateConfigSchema)->up();
		(new \CreateAppsSchema)->up();
		(new \CreateAppsMetaSchema)->up();

		\Artisan::call("migrate:install");
		\Artisan::call("migrate", ["--path" => __MIGRATE__]);

		## Seeders
		\Artisan::call("db:seed", ["--class" => "AppSeeders"]);

		return redirect()->to('install/account')
						 ->with("system_success", __("install::args.database.message.success"));

	}

}

/* End of Controller DatabaseConroller.php */