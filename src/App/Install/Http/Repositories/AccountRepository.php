<?php namespace Install\Http\Repositories;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


class AccountRepository {
	
	public function __construct() {
	
	}

	public function updatePassword($request) {

		$user 			= user("userdb")->src("admin");
		$user->password = $request->get("pass");

		if($user->save()) {
			return redirect()->to("install/finished");
		}

 		return back()->with("system_success", __("install::args.account.message.success-entity"));
	}

}

/* End of Controller AccountRepository.php */