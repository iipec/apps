<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

Route::get("/", "IndexController@index");
Route::get("/published", "IndexController@published");

Route::get("/env", "EnvController@index");
Route::post("/env", "EnvController@update");

Route::get("/database-create", "DatabaseController@index");

Route::get("/account", "AccountController@index");
Route::post("/account", "AccountController@updatePassword");

Route::get("/finished", "FinishedController@index");

Route::get("/out", "FinishedController@outInstall");
Route::get("/go-to-admin", "FinishedController@goToAdmin");

/* End of helper routes.php */