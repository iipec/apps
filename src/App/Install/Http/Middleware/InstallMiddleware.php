<?php namespace Install\Http\Middleware;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Closure;
use Library\Alerts\Support\Alert;
use Illuminate\Support\Facades\Auth;

class InstallMiddleware {

    public function handle($request, Closure $next) {

    	view()->share([
    		"language"	=> config("language", "es"),
    		"charset"	=> config("charset", "utf-8")
    	]);

        return $next($request);
    }

}

/* End of Library InstallMiddleware.php */