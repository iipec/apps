<?php namespace Install\Http\Middleware;

/**
*---------------------------------------------------------
* ©IIPEC
*---------------------------------------------------------
*/

use Apps\Accessors\MiddlewareAccessor;

class Load extends MiddlewareAccessor {
	
    protected $middleware = [
    ];

    protected $middlewareGroups = [

      "install"  => [
        
      	\App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Install\Http\Middleware\ProxyMiddleware::class,
        \Install\Http\Middleware\InstallMiddleware::class,

      ],


    ];

    protected $routeMiddleware = [
    ];

}

/* End of Library Kernel.php */