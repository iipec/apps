<?php namespace Install\Http\Middleware;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Closure;
use Illuminate\Support\Facades\Auth;

class ProxyMiddleware {

    public function handle($request, Closure $next) {

    	if(core("system")->active('package', 'install')) {
    	}

        return $next($request);
    }

}

/* End of Library ProxyMiddleware.php */