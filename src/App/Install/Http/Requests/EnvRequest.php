<?php namespace Install\Http\Requests;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Foundation\Http\FormRequest;

class EnvRequest extends FormRequest {
	
	public function authorize() {

        return true;

    }

    public function rules() {

        return [
            "env" => "required"
        ];

    }

}

/* End of EnvRequest.php */