<?php namespace Install\Http\Requests;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest {
	
	public function authorize() {

        return true;

    }

    public function rules() {

        return [
            "pass"		=> "required",
            "repass"	=> "required|same:pass"
        ];

    }

    public function messages() {

    	return [
    		"required"	=> __("install::args.account.validations.required"),
    		"same"		=> __("install::args.account.validations.same"),
    	];

    }

}

/* End of PasswordRequest.php */