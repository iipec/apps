<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

return [

	"index" => [
		"info"	=> "Hipervisor de las aplicaciones para Laravel 5.*.",

		"message"	=> [
			"success" => "Se publicaron todos los recursos correctamente",
		],
	],

	"env"	=> [

		"title"		=> "Variables de entornos.",
		"info"		=> "Configure las variables de entorno de Laravel conforme a sus necesidades.",

		"message"	=> [
			"success" => "Variables actualizadas correctamente",
		],
	],
	"database"	=> [
		"message"	=> [
			"success" => "Se crearon las entidades y se lanzaron las migraciones correctamente.",
		],
	],

	"account"	=> [
		"title"		=> "Cuenta administrativa",
		"info"		=> "Por favor defina una contraseña para la cuenta administrativa.",
		
		"form"		=> [
			"place-pass" 	=> "Su nueva contraseña",
			"place-repass"	=> "Confirme su nueva contraseña",
		],
		
		"validations" 	=> [
			"required"	=> "Campo requerido",
			"same"		=> "Las contraseña no coinciden",
		],
	],

	"finished" => [
		"title"	=> "Felicidades!",
		"info"	=> "Instalación finalizada correctamente.",

		"warning"	=> "Al salir se desautoriza el acceso al ayudante de instalación."
	],

	"brand"	=> "©IIPEC. Santo Domingo. República Dominicana.",

];

/* End of helper args.php */