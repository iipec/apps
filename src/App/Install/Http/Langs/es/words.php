<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

return [

	"install"			=> "Instalación",
	"hypervisor"		=> "Hipervisor",
	"documentation"		=> "Documentación",
	"news"				=> "Noticias",
	"env"				=> "Ambiente",
	"return"			=> "Retornar",
	"update"			=> "Actualizar",
	"entity-create"		=> "Crear entidades",
	"save"				=> "Guardar",
	"out"				=> "Salir",
	"go-to-admin"		=> "Administrar hipervisor",			

];

/* End of helper words.php */