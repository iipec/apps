<?php namespace Apps\Accessors;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Routing\Router;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;

abstract class MiddlewareAccessor extends ServiceProvider {

	protected $middleware 		= [];

	protected $middlewareGroups = [];

	protected $routeMiddleware 	= [];

	public function boot(Router $router, Kernel $kernel) {

		$this->bootMiddleware($kernel);
		$this->bootMiddlewareGroup($router);
		$this->bootMiddlewareRouter($router);

	}

	public function register(){}

	public function bootMiddleware($kernel) {

		if(!empty($this->middleware) && is_array($this->middleware)) {

			foreach ($this->middleware as $middleware) {

				$kernel->pushMiddleware($middleware);

			}

		}

	}

	public function bootMiddlewareGroup($router) {

		if(!empty($this->middlewareGroups) && is_array($this->middlewareGroups)) {

			foreach ($this->middlewareGroups as $key => $middlewares) {

				$router->middlewareGroup($key, $middlewares);

			}

		}

	}

	public function bootMiddlewareRouter($router) {

		if(!empty($this->routeMiddleware) && is_array($this->routeMiddleware)) {

			foreach ($this->routeMiddleware as $route => $middleware) {

				$router->aliasMiddleware($route, $middleware);

			}
		}
		
	}

}


/* End of accessor MiddlewareAccessor.php */