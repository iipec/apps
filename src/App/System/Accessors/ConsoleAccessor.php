<?php namespace Apps\Accessors;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Console\Kernel;

abstract class ConsoleAccessor extends ServiceProvider {

	protected $handlerPath;	

	public function boot(Kernel $kernel) {
		if(is_readable($this->handlerPath)) require_once($this->handlerPath);
	}

	public function register() {
		if(isset($this->commands) && !empty($this->commands)) $this->commands($this->commands);
	}

}


/* End of providers ConsoleAccessor.php */