<?php namespace Apps\Accessors;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


abstract class SingletonAccessor {

	private static $APP = [];

	public function singleton($alia=NULL, $class=NULL) {

		if(!empty($alia) && is_object($class)) {

			if(!array_key_exists($alia, self::$APP)) {

				return (self::$APP[$alia] = $class);

			}
		}

		abort(500, "Error, la clase alia {$alia} existe");

	}

	public function load($alia=NULL, $class=NULL) {

		if(!empty($alia) && is_object($class)) {

			if(!array_key_exists($alia, self::$APP)) {

				return (self::$APP[$alia] = $class);

			}
		}

		abort(500, "Error, la clase alia {$alia} existe");

	}

	public function app($key=NULL) {
		
		if(empty($key)) return $this;

		if(array_key_exists($key, self::$APP)) {

			return self::$APP[$key];

		}

		abort(500, "Error, la clase {$key} no existe");

	}
}


/* End of providers SingletonAccessor.php */