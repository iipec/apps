<?php namespace Apps\Accessors;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Apps\Support\Apps;
use Illuminate\Support\ServiceProvider;

abstract class AppsAccessor extends ServiceProvider {

	public function boot() {

		## LANGUAGE
		$this->app->setLocale(config("language", "es"));

		$this->publishes([

			__path("Storage/Public/Apps") => public_path("apps"),

	    ], "apps");

	}

	public function register() {

		$this->app->bind("Apps", function($app) {

			return new \Apps\Support\AppsSupport($app);

		});

		Apps::singleton("system", new \Apps\Support\Libraries\System);
		Apps::singleton("config", new \Apps\Models\Config);
		Apps::singleton("apps", new \Apps\Models\AppsModel);
		Apps::singleton("meta", new \Apps\Models\AppsMetaModel);

		require_once(__DIR__."/../Support/AppsCommon.php");

		$this->app["hypervisor"] = Apps::app();


		if(!core("system")->has() OR core("system")->active("package", "install") OR !core("system")->active() ) {

			Apps::loadKernel(new \Install\Kernel);

		}

		if(method_exists($this, "system") && core("system")->has() OR core("system")->active()) {

			$this->system(Apps::app());

		}

	}
	
}


/* End of providers AppsAccessor.php */