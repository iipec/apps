<?php namespace Apps\Support\Traits;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

trait MountedTrait {
	
	public function mountConfigs() {
		$this->app("system")->setConfigs();
	}

	public function mountPackages() {

		if( ($packages = $this->get("package"))->count() > 0 ) {
			
			foreach ($packages as $row) {
				$this->loadKernel($row->kernel);
			}

		}

	}

	public function mountPlugins() {

		if( !empty( ($plugins = $this->get("plugin")) ) ) {
			$this->loadComponent($plugins);
		}

	}

	public function mountLibraries() {
		
		if( !empty( ($libraries = $this->get("library")) ) ) {
			$this->loadComponent($libraries);
		}

	}

	public function mountThemes() {

		if( !empty( ($themes = $this->get("theme")) ) ) {
			$this->loadComponent($themes);
		}

	}

	public function mountWidgets() {

		if( !empty( ($widgets = $this->get("widget")) ) ) {
			$this->loadComponent($widgets);
		}

	}
	
}

/* End of Library Mounted.php */