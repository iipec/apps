<?php namespace Apps\Support\Traits;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

trait BootstrapTrait {

	public function get($type=NULL) {
		return $this->app("system")->get($type);
	}
	
	public function getBootstrap() {
		
	}

}

/* End of Library Bootstrap.php */