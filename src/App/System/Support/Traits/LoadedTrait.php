<?php namespace Apps\Support\Traits;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Foundation\AliasLoader;

trait LoadedTrait {
	
	public function loadProviders($providers=[]) {

		if(empty($providers)) return NULL;

		if(!is_array($providers)) $providers = [$providers];

		foreach ($providers as $provider) {
			
			$this->app->register($provider);

		}

	}

	public function loadAlias($alias=[]) {

		if(empty($alias) OR !is_array($alias)) return NULL;

		foreach ($alias as $alia => $class) {

			AliasLoader::getInstance()->alias($alia, $class);

		}

	}

	public function loadKernel($kernel=NULL) {

		if(!empty($kernel)) {

			if(!is_object($kernel)) $kernel = new $kernel;

			## [0]
			if(method_exists($kernel, "handler")) $kernel->handler($this->app);

			## [1]
			if(method_exists($kernel, "providers")) $this->loadProviders($kernel->providers());

			## [2]
			if(method_exists($kernel, "alias")) $this->loadAlias($kernel->alias());

			return $kernel;

		}

		abort(500, "Error kernel packages");

	}

	public function loadComponent($components=NULL) {

		if(empty($components)) return $components;

		foreach ($components as $component) {
			
			if(!empty( ($provider = $component->provider) )) {
				$this->loadProviders($provider);
			}

			if(!empty( ($alia = $component->slug) ) && !empty( ($facade = $component->facade) )) {
				$this->loadAlias([ucwords($alia) => $facade]);
			}				

		}

	}

	public function loadPackages() {

	}

}

/* End of Library LoadedTrait.php */