<?php namespace Apps\Support\Libraries;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use \DB;
use \Schema;

class System {
	
	public function __construct() {
	
	}

	public function has() {
		
		return (Schema::hasTable("apps") && Schema::hasTable("apps_meta"));

	}

	public function active($type="core", $slug="hypervisor") {

		if($this->has()) {

			if( ($query = DB::table("apps")->where("type", $type)->where("slug", $slug))->count() > 0 ) {

				return $query->first()->activated;
				
			}

		}

		return FALSE;
	}

	public function setConfigs() {

		if( ($query = DB::table("configs")->where("activated", 1))->count() > 0 ) {

			foreach ($query->get() as $row) {

				config()->set($row->key, $row->value);

			}

		}

	}

	public function get($type=NULL) {

		if($this->has()) {

			if( ($query = DB::table("apps")->where("type", $type)->where("activated", 1))->count() > 0 ) {

				return $query->get();
				
			}

		}

		return NULL;

	}

}

/* End of Controller System.php */