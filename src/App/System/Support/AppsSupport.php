<?php namespace Apps\Support;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


use Apps\Accessors\SingletonAccessor;
use Apps\Support\Traits\BootstrapTrait;
use Apps\Support\Traits\LoadedTrait;
use Apps\Support\Traits\MountedTrait;

class AppsSupport extends SingletonAccessor {
	
	use BootstrapTrait, LoadedTrait, MountedTrait;

	protected $app;

	protected $loaded = [];

	protected $proxy = [
		"config", 
		"core", 
		"packages", 
		"plugins", 
		"libraries", 
		"themes", 
		"widgets"
	];

	public function __construct($app) {

		$this->app = $app;
	
	}

	public function info() {
		return 'Info';
	}

}

/* End of Controller AppsSupport.php */