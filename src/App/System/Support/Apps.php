<?php namespace Apps\Support;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Facade;

class Apps extends Facade {

	public static function getFacadeAccessor() {

		return "Apps";
		
	}
	
}

/* End of Facade Apps.php */