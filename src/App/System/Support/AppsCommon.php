<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

/*
* HELPERS PATH */

if(!function_exists("__path")) {

	function __path($path=NULL) {

		return realpath(__DIR__."/../../../")."/".trim($path, "/");

	}

}

if(!function_exists("library_path")) {

	function library_path($path=NULL) {
		return __path("Storage/Apps/Libraries/".trim($path, "/"));
	}

}

if(!function_exists("plugin_path")) {

	function plugin_path($path=NULL) {
		return __path("Storage/Apps/Plugins/".trim($path, "/"));
	}
	
}

if(!function_exists("theme_path")) {

	function theme_path($path=NULL) {
		return __path("Storage/Apps/Themes/".trim($path, "/"));
	}
	
}

if(!function_exists("widget_path")) {

	function widget_path($path=NULL) {
		return __path("Storage/Apps/Widgets/".trim($path, "/"));
	}
	
}


/*
* CONSTANTS */
define('__APPS__', __path());
define('__MIGRATE__', str_replace(base_path(), NULL, __path("Storage/Database/Migrations")));


/*
* CORE HELPERS */

if(!function_exists("core")) {

	function core($key=NULL) {

		return \Apps\Support\Apps::app($key);

	}

}

/* End of helper AppsCommon.php */