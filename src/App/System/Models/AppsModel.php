<?php namespace Apps\Models;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Apps\Models\AppsMetaModel;
use Illuminate\Database\Eloquent\Model;

class AppsModel extends Model {
	
	protected $table = "apps";

	protected $fillable = [
		"type", 
		"slug", 
		"kernel", 
		"provider", 
		"facade", 
		"token", 
		"activated", 
		"created_at", 
		"updated_at"
	];

	public function meta() {
		return $this->hasOne(AppsMetaModel::class, "app_id");
	}

	public function addMeta($data=NULL) {

    	if(!empty($data) && is_array($data)) {

    		$this->meta()->create($data);

    	}

    	return $this;
    }

    public function src($type=NULL, $slug=NULL) {

    	if( ($query = $this->where("type", $type)->where("slug", $slug))->count() > 0 ) {

    		return $query->first();

    	}

    	return $this;
    }

}

/* End of Model AppsModel.php */