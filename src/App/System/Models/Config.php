<?php namespace Apps\Models;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;

class Config extends Model {
	
	protected $table = "configs";

	protected $fillable = [
		"key", "value", "activated"
	];

	public $timestamps = false;


	public function add($data=NULL) {

		if(!empty($data) && is_array($data)) {

			foreach ($data as $key => $value) {
				
				$this->create(["key" => $key, "value" => $value]);

			}
		}

		return $this;
	}

	public function set($key=NULL, $value=NULL) {

		if(!empty($key) && !empty($value)) {

			if( ($query = $this->where("key", $key))->count() > 0) {

				$param = $query->first();
				$param->value = $value;

				if($param->save()) {

					return TRUE;

				}
			}

			return FALSE;
		}
	}

}

/* End of Model Config.php */