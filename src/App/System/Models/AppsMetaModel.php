<?php namespace Apps\Models;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;

class AppsMetaModel extends Model {
	
	protected $table = "apps_meta";

	protected $fillable = [
		"name", "author", "email", "license", "site", "version", "description"
	];

	public $timestamps = false;

}

/* End of Model AppsMetaModel.php */