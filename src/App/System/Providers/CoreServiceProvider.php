<?php namespace Apps\Providers;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Apps\Accessors\AppsAccessor;

class CoreServiceProvider extends AppsAccessor {

	protected $path;

	public function system($app) {

		/*
		*  CONFIGS
		*  [0] Monts parameters */
		$this->app["hypervisor"]->mountConfigs();

		/*
		*  PACKAGES
		*  [1] Monts packages kernels */
		$this->app["hypervisor"]->mountPackages();

		/*
		*  PLUGINS
		*  [2] Monts plugins */

		$this->app["hypervisor"]->mountPlugins();

		/*
		*  LIBRARIES
		*  [3] Monts Libraries */
		$this->app["hypervisor"]->mountLibraries();

		/*
		*  THEMES
		*  [4] Monts themes */
		$this->app["hypervisor"]->mountThemes();

		/*
		*  WIDGETS
		*  [5] Monts widgets */
		$this->app["hypervisor"]->mountWidgets();


	}
	

}


/* End of providers CoreServiceProvider.php */