<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsRolsSchema extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('groups_rols', function (Blueprint $table) {
            
            $table->increments('id');

            $table->integer('rol_id')->unsigned();
            $table->foreign('rol_id')->references('id')->on('rols')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->engine = 'InnoDB';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('groups_rols');
    }
}
