<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersInfoSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('users_info', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->string("firstname");
            $table->string("lastname");
            $table->date("birthday")->nullable();
            $table->string("place_birth")->nullable();
            $table->string("nationality")->nullable();
            $table->string("dni")->nullable();

            $table->string("current_address")->nullable();
            $table->string("street")->nullable();
            $table->string("no")->nullable();
            $table->string("sector")->nullable();
            $table->string("home_phone")->nullable();
            $table->string("cellphone")->nullable();
            $table->string("office_phone")->nullable();

            $table->string("blood_type")->nullable();
            $table->text("allergic_to")->nullable();
            $table->text("medical_note")->nullable();

            $table->string("civil_status")->nullable();
            $table->string("ocupation")->nullable();
            $table->string("work_place")->nullable();
            
            $table->timestamps();

            $table->engine = 'InnoDB';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::dropIfExists('users_info');
        
    }
}
