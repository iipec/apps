<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersSchema extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('users', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->string("public_name")->default("Owner");

            $table->string("user", 30)->unique();
            $table->string("email", 100)->unique();

            $table->string("password");
            
            $table->string("avatar")->default("contents/users/default/user.png");
            $table->string("type")->default("local");

            $table->char("activated", 1)->default(0);

            $table->rememberToken();

            $table->timestamps();

            $table->engine = 'InnoDB';

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
