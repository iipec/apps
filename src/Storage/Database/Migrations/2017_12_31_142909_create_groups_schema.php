<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsSchema extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('groups', function (Blueprint $table) {
            
            $table->increments('id');

            $table->integer("parent")->default(0);
            
            $table->string("slug")->unique();

            $table->text("description")->nullable();

            $table->integer("counts")->default(0);

            $table->boolean("activated")->default(1);

            $table->engine = 'InnoDB';

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('groups');
    }
}
