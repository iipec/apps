<?php

use Illuminate\Support\Facades\Schema;

class CreateAppsMetaSchema {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('apps_meta', function ($table) {
            
            $table->bigIncrements('id');

            $table->integer('app_id')->unsigned();
            $table->foreign('app_id')->references('id')->on('apps')->onDelete('CASCADE')->onUpdate('CASCADE');
            
            $table->string("name", 30);
            $table->string("author", 100)->nullable();
            $table->string("email", 80)->nullable();
            $table->string("license", 20)->nullable();
            $table->text("site")->nullable();
            $table->string("version", 20)->nullable();
            $table->text("description")->nullable();

            $table->boolean("activated")->default(1);
            $table->boolean("pingable")->default(0);

            $table->engine = 'InnoDB';

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('apps_meta');
    }
}