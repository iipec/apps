<?php

use Illuminate\Support\Facades\Schema;

class CreateConfigSchema {
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {
       
        Schema::create('configs', function ($table) {

            $table->bigIncrements('id');

            $table->string("key");
            $table->text("value");

            $table->boolean("activated")->default(1);

            $table->engine = 'InnoDB';

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('configs');
    }
}