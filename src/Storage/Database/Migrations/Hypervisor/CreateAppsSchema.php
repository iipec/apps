<?php

use Illuminate\Support\Facades\Schema;

class CreateAppsSchema {
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {
       
        Schema::create('apps', function ($table) {

            $table->increments('id');

            $table->string("type");
            $table->string("slug", 30);

            $table->text("kernel")->nullable();
            $table->text("provider")->nullable();
            $table->text("facade")->nullable();

            $table->text("token")->nullable();

            $table->char("activated", 1)->default(0);

            $table->timestamps();

            $table->engine = 'InnoDB';

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('apps');
    }
}