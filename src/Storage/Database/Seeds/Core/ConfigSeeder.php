<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Apps\Models\Config;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder {
	
	public function run() {
        
        core("config")->add([
        	
        	"charset"                       => "utf-8",
    		"language" 						=> "es",

			"admin.theme"					=> "rose",
            "admin.skin"                    => "master",

			"mail.from.address" 			=> "info@iipec.net",
			"mail.from.name" 				=> "©IIPEC",

			"auth.guards.admin.driver"		=> "session",
			"auth.guards.admin.provider"	=> "admin",
			"auth.providers.admin.driver" 	=> "eloquent",
			"auth.providers.admin.model" 	=> \Library\Users\Models\Account\UserModel::class,
			"auth.providers.users.model"	=> \Library\Users\Models\Account\UserModel::class,

        ]);

    }

}

/* End of seeds ConfigSeeder.php */