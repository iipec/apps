<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class LibrarySeeder extends Seeder {
	
	public function run() {
        
        $apps = core("apps");

        (new \Library\Alerts\Info)->handler($apps);
        (new \Library\Users\Info)->handler($apps);
        (new \Library\Menu\Info)->handler($apps);
        (new \Library\Skin\Info)->handler($apps);

    }

}

/* End of seeds LibrarySeeder.php */