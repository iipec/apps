<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder {
	
	public function run() {
        
        $apps = core("apps");
        
        (new \Install\Info)->handler($apps);
        (new \Admin\Info)->handler($apps);

    }

}

/* End of seeds PackageSeeder.php */