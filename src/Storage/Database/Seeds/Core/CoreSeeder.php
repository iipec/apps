<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Apps\Models\AppsModel;
use Illuminate\Database\Seeder;

class CoreSeeder extends Seeder {
	
	public function run() {

		$apps = core("apps");
        
        (new \Apps\Info)->handler($apps);

    }

}

/* End of seeds CoreSeeder.php */