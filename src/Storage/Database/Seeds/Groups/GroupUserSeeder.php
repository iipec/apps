<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class GroupUserSeeder extends Seeder {
	
	public function run() {
        
        /*
        * SYSTEM */
        group("groupdb")->create([
        	"slug" => "system", "description" => "Sistema"
        ]);

        ## Sub system
        user("groupdb")->create([
            "parent"        => "system", 
            "slug"          => "admin", 
            "description"   => "Administrador"
        ]);

        user("groupdb")->create([
            "parent"            => "system", 
            "slug"              => "superuser", 
            "description"       => "Supervisor"
        ]);

        user("groupdb")->create([
            "parent"        => "system", 
            "slug"          => "user", 
            "description"   => "Usuario"
        ]);

        /*
        * COLEGIO LOYOLA */
        group("groupdb")->create([
        	"slug" => "loyolasd", "description" => "Colegio Loyola"
        ]);

        ## DEPARMETS
        group("groupdb")->add([
            ["parent" => "loyolasd", "slug" => "administrative",  "description" => "Administrativos"],
            ["parent" => "loyolasd", "slug" => "academics",  "description"      => "Academicos"],
            ["parent" => "loyolasd", "slug" => "students",  "description"       => "Estudiantes"],
            ["parent" => "loyolasd", "slug" => "family",  "description"         => "Familias"],
            ["parent" => "loyolasd", "slug" => "admissions",  "description"     => "Admisiones"]
        ]);

        ## Administrative
        group("groupdb")->add([
            ["parent" => "administrative", "slug" => "rectoria",  "description"  => "Rectoria"],
            ["parent" => "administrative", "slug" => "security",  "description"  => "Seguridad"],
            ["parent" => "administrative", "slug" => "rrhh",  "description"      => "Recursos Humanos"],
            ["parent" => "administrative", "slug" => "finance",  "description"   => "Finanzas"],
            ["parent" => "administrative", "slug" => "tic",  "description"       => "Tecnología"],     
            ["parent" => "administrative", "slug" => "registry",  "description"  => "Registro"],            
            ["parent" => "administrative", "slug" => "reception",  "description" => "Recepcion"],
        ]);

        ## Academic
        group("groupdb")->add([
            ["parent" => "academics", "slug" => "coordinations",  "description"   => "Coordinaciónes"],
            ["parent" => "academics", "slug" => "teachers",  "description"        => "Docentes"],
            ["parent" => "academics", "slug" => "psychology",  "description"      => "Psicología"],
        ]);

    }

}

/* End of seeds GroupUserSeeder.php */