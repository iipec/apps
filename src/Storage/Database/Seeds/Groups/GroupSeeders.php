<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class GroupSeeders extends Seeder {
	
	public function run() {
        
        $this->call(GroupUserSeeder::class);

    }

}

/* End of seeds GroupSeeders.php */