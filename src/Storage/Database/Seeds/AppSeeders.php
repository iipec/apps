<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class AppSeeders extends Seeder {
	
	public function run() {
        
        $this->call(ConfigSeeder::class);
        $this->call(CoreSeeder::class);
        $this->call(PackageSeeder::class);
        $this->call(LibrarySeeder::class);

        //$this->call(GroupSeeders::class);

        $this->call(UserSeeder::class);

    }

}

/* End of seeds AppSeeders.php */