<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
	
	public function run() {
        
        $this->call(UserRolSeeder::class);
        $this->call(UserGroupSeeder::class);
        $this->call(UserAdminSeeder::class);

    }

}

/* End of seeds UserSeeder.php */