<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class UserRolSeeder extends Seeder {
	
	public function run() {
        
        user("roldb")->add([
        	["rol" => "view", "description" => "Ver"],
        	["rol" => "edit", "description" => "Editar"],
        	["rol" => "delete", "description" => "Eliminar"],        	
        ]);

    }

}

/* End of seeds UserRolSeeder.php */