<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder {
	
	public function run() {
        
        user("userdb")->create([

        	"public_name" 	=> "Sys Admin",
        	"user"			=> "admin",
        	"email"			=> "admin@iipec.net",
        	"password"		=> "admin",
        	"activated"		=> 1,

        ])->addinfo([

        	"firstname"	=> "Sys",
        	"lastname"	=> "Admin"
            
        ])->syncGroup(["admin","superuser","user"]);

    }

}

/* End of seeds UserAdminSeeder.php */