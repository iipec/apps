<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Seeder;

class UserGroupSeeder extends Seeder {
	
	public function run() {
        
        ## System Entity
        user("groupdb")->create([ "slug" => "system", "description" => "Sistema"]);

        user("groupdb")->create(["parent" => "system", "slug" => "admin", "description" => "Administrador"])
        			   ->syncRol(["view", "edit", "delete"]);

        user("groupdb")->create(["parent" => "system","slug" => "superuser","description" => "Supervisor"])
        			   ->syncRol(["view", "edit"]);

        user("groupdb")->create(["parent" => "system","slug" => "user","description" => "Usuario"])
        			   ->syncRol(["view"]);

        ## Loyola Entity
        user("groupdb")->create(["slug" => "loyolasd", "description" => "Colegio Loyola"]);

        # Administrative 
        user("groupdb")->create(["parent"=>"loyolasd", "slug" => "administrative", "description" => "Administrativos"]);
        user("groupdb")->add([
            ["parent" => "administrative", "slug" => "rectoria",  "description"  => "Rectoría"],
            ["parent" => "administrative", "slug" => "security",  "description"  => "Seguridad"],
            ["parent" => "administrative", "slug" => "rrhh",  "description"      => "Recursos Humanos"],
            ["parent" => "administrative", "slug" => "finance",  "description"   => "Finanzas"],
            ["parent" => "administrative", "slug" => "tic",  "description"       => "Tecnología"],     
            ["parent" => "administrative", "slug" => "registry",  "description"  => "Registro"],            
            ["parent" => "administrative", "slug" => "reception",  "description" => "Recepción"],
        ], ['view']);

        # Academicos 
        user("groupdb")->create(["parent" => "loyolasd","slug" => "academics","description" => "Academicos"]);
        user("groupdb")->add([
            ["parent" => "academics", "slug" => "coordinations",  "description"   => "Coordinaciónes"],
            ["parent" => "academics", "slug" => "teachers",  "description"        => "Docentes"],
            ["parent" => "academics", "slug" => "psychology",  "description"      => "Psicología"],
        ], ['view']);

        # Family 
        user("groupdb")->create(["parent" => "loyolasd","slug" => "family","description" => "Familias"]);
        user("groupdb")->add([
            ["parent" => "family", "slug" => "father","description" => "Padre"],
            ["parent" => "family", "slug" => "mother","description" => "Madre"],
            ["parent" => "family", "slug" => "tutor", "description" => "Tutor"],
        ], ['view']);
        
    }

}

/* End of seeds UserGroupSeeder.php */