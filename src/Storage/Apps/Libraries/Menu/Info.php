<?php namespace Library\Menu;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

class Info {

	public function apps() {

		return [

			"type"			=> "library",
			"slug"			=> "menu",
			"provider"		=> \Library\Menu\MenuServiceProvider::class,
			"facade"		=> \Library\Menu\Menu::class,
			"token"			=> NULL,
			"activated" 	=> 1,

		];

	}

	public function meta() {

		return [
			
			"name"			=> "Menu",
			"author"		=> "Ing. Ramón A Linares Febles",
			"email"			=> "rlinares4381@gmail.com",
			"license"		=> "MIT",
			"site"			=> "http://www.iipec.net",
			"version"		=> "V-1.0",
			"description" 	=> "Librería de los menu.",

		];

	}

	public function handler($apps) {
		
		$apps->create($this->apps())->addMeta($this->meta());

	}

}

/* End of Info.php */