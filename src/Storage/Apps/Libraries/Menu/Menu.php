<?php namespace Library\Menu;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Facade;

class Menu extends Facade {

	public static function getFacadeAccessor() {

		return "Menu";
		
	}
	
}

/* End of Facade Menu.php */