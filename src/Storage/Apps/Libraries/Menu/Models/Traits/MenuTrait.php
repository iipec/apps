<?php namespace Library\Menu\Models;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

trait MenuTrait {

	public function ID($slug=NULL) {

		if(empty($slug)) return NULL;

		if( ($query = $this->where("slug", $slug)->where("activated", 1))->count() > 0 ) {

			return $query->first()->id;

		}

		return NULL;

	}
	
	public function getBySlug($slug=NULL) {

		if(empty($slug)) return NULL;

		if( ($query = $this->where("slug", $slug)->where("activated", 1))->count() > 0 ) {

			return $this->first();

		}

		return NULL;

	}

	public function getParent($ID=NULL, $parent=0) {

		if(empty($ID) OR !is_numeric($parent)) return NULL;

		if( ($query = $this->find($ID)->link()->where("parent", $parent)->where("activated", 1))->count() > 0 ) {

			return $query->get();

		}

		return NULL;

	}

	public function addLinks($data=NULL) {

		if(empty($data) OR !is_array($data)) return NULL;

		foreach ($data as $row) {

			$this->link()->create($row);
		}

		return $this;

	}

}

/* End of Library MenuTrait.php */