<?php namespace Library\Menu\Models;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;

class Link extends Model {
	
	protected $table = "links";

	protected $fillable = [
		"menu_id",
		"parent",
		"label",
		"url",
		"icon",
		"activated",
		"created_at",
		"updated_at"
	];

	public function ID($label=NULL) {

		if(empty($label)) return NULL;

		if( ($query = $this->where("label", $label))->count() > 0 ) {

			return $query->first()->id;

		}

		return NULL;
	}

	public function setParentAttribute($value) {

        return $this->attributes['parent'] = $this->ID($value);

    }

}

/* End of Model Link.php */