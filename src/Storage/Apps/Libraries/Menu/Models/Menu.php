<?php namespace Library\Menu\Models;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Library\Menu\Models\MenuTrait;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model {
	
	use MenuTrait;

	protected $table = "menus";

	protected $fillable = [
		"name", "slug", "activated", "created_at", "updated_at"
	];

	public function link() {
		return $this->hasMany(\Apps\Models\Menus\Link::class, "menu_id");
	}

}

/* End of Model Menu.php */