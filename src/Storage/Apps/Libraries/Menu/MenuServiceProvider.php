<?php namespace Library\Menu;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Library\Menu\Menu;
use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider {

	public function boot() {

	}

	public function register() {

		$this->app->bind("Menu", function($app) {

			return new \Library\Menu\MenuSupport($app);

		});

		## DB
		Menu::singleton("menudb", new \Library\Menu\Models\Menu);

		## MENU
		Menu::singleton("bootstrap", new \Library\Menu\Supports\Bootstrap);

		require_once(__DIR__."/MenuCommon.php");

	}

}


/* End of providers MenuServiceProvider.php */