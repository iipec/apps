<?php namespace Library\Menu\Supports;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


use Library\Menu\Models\Menu;

class Bootstrap extends MenuAccessor {

	
	
	public function __construct() {
		
		parent::boot(new Menu);

	}

	public function nav($attrs=NULL, $index = 4) {

		$tag = NULL;

		if( !empty(($P0 = $this->parent())) ):

		$tag = __ident($index);	

		$tag .= '<ul'.__attrs($attrs).'>'."\n";


			foreach ($P0 as $X0):
				
				if( !empty( ($P1 = $this->parent($X0->id)) ) ):

					$tag .= __ident($index + 4);
					$tag .= '<li class="dropdow">'."\n";

					$tag .= __ident($index + 8);
					$tag .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">'."\n";

					$tag .= __ident($index + 12);
					$tag .= __iconice($X0->icon);
					$tag .= __("admin::words.$X0->label");
					$tag .= ' <i class="caret"></i>'."\n";

					$tag .= __ident($index + 8);
					$tag .= '</a>'."\n";

					$tag .= __ident($index + 8);
					$tag .= '<ul class="dropdown-menu">'."\n";

					foreach($P1 as $X1):

						if( !empty( ($P2 = $this->parent($X1->id)) ) ):

							$tag .= __ident($index + 4);
							$tag .= '<li class="dropdow">'."\n";

							$tag .= __ident($index + 8);
							$tag .= '<ul class="dropdown-menu">'."\n";

							foreach ($P2 as $X2):

								if( !empty( ($P3 = $this->parent($X2->id)) ) ):

								else:	

									$tag .= __ident($index + 12);
									$tag .= '<li>'."\n";

									$tag .= __ident($index + 16);
									$tag .= '<a href="'.$X2->url.'>'."\n";

									$tag .= __ident($index + 20);
									$tag .= __iconice($X2->icon);
									$tag .= __("admin::words.$X1->label")."\n";

									$tag .= __ident($index + 16);
									$tag .= "</a>\n";

									$tag .= __ident($index + 12);
									$tag .= "</li>\n";								

								endif;

							endforeach;
							$tag .= __ident($index + 8);
							$tag .= "</ul>\n";

							$tag .= __ident($index + 4);
							$tag .= "</li>\n";

						else:

							$tag .= __ident($index + 12);
							$tag .= '<li>'."\n";

							$tag .= __ident($index + 16);
							$tag .= '<a href="'.$X1->url.'>'."\n";

							$tag .= __ident($index + 20);
							$tag .= __iconice($X1->icon);
							$tag .= __("admin::words.$X1->label")."\n";

							$tag .= __ident($index + 16);
							$tag .= "</a>\n";

							$tag .= __ident($index + 12);
							$tag .= "</li>\n";

						endif;

					endforeach;

					$tag .= __ident($index + 8);
					$tag .= "</ul>\n";

					$tag .= __ident($index + 4);
					$tag .= "</li>\n";

				else:

					$tag .= __ident($index + 4);
					$tag .= '<li>'."\n";

					$tag .= __ident($index + 8);
					$tag .= '<a href="'.$X1->url.'>'."\n";

					$tag .= __ident($index + 12);
					$tag .= __iconice($X1->icon);
					$tag .= __("admin::words.$X1->label")."\n";

					$tag .= __ident($index + 8);
					$tag .= "</a>\n";

					$tag .= __ident($index + 4);
					$tag .= "</li>\n";

				endif;

			endforeach;		

		$tag .= __ident($index);
		$tag .= "</ul>\n";

		endif;

		return $tag;

	}

}

/* End of Controller Bootstrap.php */