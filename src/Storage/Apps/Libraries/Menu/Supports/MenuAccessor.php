<?php namespace Library\Menu\Supports;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Library\Menu\Models\Menu;

abstract class MenuAccessor {

	protected $ID;

	protected $name;

	protected $slug;

	protected $menu;

	public function boot(Menu $menu) {

		$this->menu = $menu;

	}

	public function menu($slug=NULL) {

		if(empty($slug)) return $this;

		if(!empty( ($query = $this->menu->getBySlug($slug)) ) ) {

			$menu = $query->first();

			$this->ID = $menu->id;

			$this->name = $menu->name;

			$this->slug = $slug;

		}

		return $this;
	}

	public function parent($index=0) {

		return $this->menu->getParent($this->ID, $index);
		
	}



}


/* End of providers MenuAccessor.php */