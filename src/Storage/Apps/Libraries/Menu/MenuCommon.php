<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

if(!function_exists("menu")) {

	function menu($key) {

		return Library\Menu\Menu::app($key);

	}

}

if(!function_exists("__attrs")) {

	function __attrs($attrs=NULL) {

		if(empty($attrs)) return NULL;
		if(is_string($attrs)) return ' '.$attrs;

		if(is_array($attrs)) {

			$attr = NULL;

			foreach ($attrs as $key => $value):
				$attr .= ' '.$key.'="'.$value.'"';  
			endforeach;

			return $attr;

		}

		return NULL;
	}
}

if(!function_exists("__iconice")) {

	function __iconice($icon=NULL) {

		if(preg_match('/^mdi/', $icon)) return '<i class"'.$icon.'"><i> ';
		if(preg_match('/^glyphicon/', $icon)) return '<span class"'.$icon.'"><span> ';
		if(preg_match('/[jpg|png]/i', $icon)) return '<img src="'.$icon.'" alt="Image"> ';

		return NULL;

	}

}


if(!function_exists("__ident")) {

	function __ident($index=0) {

		return str_repeat(' ', $index);

	}
	
}

/* End of helper MenuCommon.php */