<?php namespace Library\Menu;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


class MenuSupport {

	private static $APP = [];
	
	public function __construct() {
	
	}

	public function singleton($alia=NULL, $class=NULL) {

		abort_IF(empty($alia) OR !is_object($class), 500, "Error parametros de la clase");

		if(!array_key_exists($alia, self::$APP)) {
			
			return self::$APP[$alia] = $class;

		}

		abort(500, "La clase {$alia} existe");

	}

	public function app($key=NULL) {

		abort_IF(empty($key), 500, "Empty key", ["Line"=>__LINE__, "Method" => __METHOD__]);

		if(array_key_exists($key, self::$APP)) {

			return self::$APP[$key];

		}

		abort(500, "La clase {$key} no existe");
		
	}

	public function make($key=NUll) {
		return 'Info';
	}

}

/* End of Controller MenuSupport.php */