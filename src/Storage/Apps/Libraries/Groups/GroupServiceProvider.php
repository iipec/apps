<?php namespace Library\Groups;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Library\Groups\Support\Group;
use Illuminate\Support\ServiceProvider;

class GroupServiceProvider extends ServiceProvider {

	public function boot() {
		
	}

	public function register() {

		$this->app->bind("Group", function($app) {

			return new \Library\Groups\Support\GroupSupport($app);

		});

		Group::load("groupdb", new \Library\Groups\Models\Group);

		require_once(__DIR__."/Support/GroupCommon.php");

	}

}


/* End of providers GroupServiceProvider.php */