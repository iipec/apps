<?php namespace Library\Groups\Models;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Library\Groups\Models\GroupUser;
use Illuminate\Database\Eloquent\Model;

class Group extends Model {
	
	protected $table = "groups";

	protected $fillable = [
		"parent", 
		"slug", 
		"description", 
		"count", 
		"activated"
	];

	public $timestamps = false;

	public function setParentAttribute($value) {

		if( !is_numeric($value) && is_numeric( ($ID = $this->src($value)->id) ) ) {

			return $this->attributes['parent'] = $ID;

		}

        return $this->attributes['parent'] = $value;

    }

	public function users() {
		return $this->belongsToMany(GroupUser::class, "groups_users", "group_id", "user_id");
	}

	public function ID($slug=NULL) {

		return $this->src($slug)->id;

	}
	
	public function src($slug=NULL) {

		if(empty($slug)) return NULL;

		if( ($query = $this->where("id", $slug)->orWhere("slug", $slug))->count() > 0 ) {

			return $query->first();

		}

		return NULL;
	}

	public function add($data=NULL, $rols=NULL) {

		if(!empty($data) && is_array($data)) {

			$toggles = NULL;

			foreach ($data as $row) {
				
				if(empty($rols)) {
					$this->create($row);
				}
				else {

					$this->create($row)->syncRol($rols);

				}

			}

		}

		return $this;
	}

}

/* End of Model Group.php */