<?php namespace Library\Groups\Models;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model {
	
	protected $table = "users";

	protected $fillable = [
		"id", 
		"public_name", 
		"user", 
		"email", 
		"password", 
		"avatar",
		"type", 
		"activated", 
		"created_at", 
		"updated_at"
	];

}

/* End of Model GroupUser.php */