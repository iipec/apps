<?php namespace Library\Groups;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

class Info {

	public function apps() {

		return [

			"type"			=> "library",
			"slug"			=> "groups",
			"provider"		=> \Library\Groups\GroupServiceProvider::class,
			"facade"		=> \Library\Groups\Supports\Group::class,
			"token"			=> NULL,
			"activated" 	=> 1,

		];

	}

	public function meta() {

		return [
			
			"name"			=> "Name",
			"author"		=> "Ing. Ramón A Linares Febles",
			"email"			=> "rlinares4381@gmail.com",
			"license"		=> "MIT",
			"site"			=> "http://www.iipec.net",
			"version"		=> "V-1.0",
			"description" 	=> "Librería de gestión de grupos.",

		];

	}

	public function handler($apps) {
		
		$apps->create($this->apps())->addMeta($this->meta());

	}

}

/* End of Info.php */