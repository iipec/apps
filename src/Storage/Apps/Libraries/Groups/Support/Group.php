<?php namespace Library\Groups\Support;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Facade;

class Group extends Facade {

	public static function getFacadeAccessor() {

		return "Group";
		
	}
	
}

/* End of Facade Group.php */