<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


if(!function_exists("group")) {

	function group($key=NULL) {

		return Library\Groups\Support\Group::app($key);

	}

}



/* End of helper GroupCommon.php */