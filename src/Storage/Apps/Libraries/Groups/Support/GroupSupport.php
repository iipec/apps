<?php namespace Library\Groups\Support;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


use Apps\Accessors\SingletonAccessor;

class GroupSupport extends SingletonAccessor {
	
	public function __construct() {
	
	}

	public function info() {
		return 'Info';
	}

}

/* End of Controller GroupSupport.php */