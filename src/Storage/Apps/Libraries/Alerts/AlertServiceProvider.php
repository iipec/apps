<?php namespace Library\Alerts;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Library\Alerts\Support\Alert;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class AlertServiceProvider extends ServiceProvider {

	public function boot() {

		$this->loadViewsFrom(__DIR__.'/Template', 'alert');

	}

	public function register() {

		$this->app->bind("Alert", function($app){

			return new \Library\Alerts\Support\AlertSupport($app);

		});

	}

}


/* End of providers AlertServiceProvider.php */