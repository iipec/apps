<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

if(!function_exists("alert")) {

	function alert($key=NULL) {

		return Library\Alerts\Support\Alert::load($key);
		
	}

}

/* End of helper AlertCommon.php */