<?php namespace Library\Alerts\Support;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Event;
use Apps\Accessors\SingletonAccessor;

class AlertSupport extends SingletonAccessor {
	
	public function __construct() {	
	}

	public function prefix($prefix=NULL) {
		return new  \Library\Alerts\Support\PrefixAlert($prefix);
	}

}

/* End of Controller AlertSupport.php */