<?php namespace Library\Alerts\Support;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Event;

class PrefixAlert {

	protected $prefix;

	protected $icons = [
		"success" 	=> '<i class="mdi mdi-check-all mdi-x24"></i>',
		"info" 		=> '<i class="mdi mdi-information-outline mdi-x24"></i>',
		"warning" 	=> '<i class="mdi mdi-alert mdi-x20"></i>',
		"error" 	=> '<i class="mdi mdi-alert-octagon mdi-x24"></i>',
	];

	protected $alerts = [];
	
	public function __construct($prefix) {
		
		$this->prefix = $prefix;

		foreach (["success", "info", "warning", "error"] as $state) {

			if( session()->has( ($alert = $prefix.'_'.$state) ) )  {

				$this->alerts["prefix"]		= $prefix;
				$this->alerts["state"]		= ($state != "error")? $state : "danger";
				$this->alerts["title"]		= ucwords($state);
				$this->alerts["icon"]		= $this->getIcon($state);
				$this->alerts["message"]	= session()->get($alert);

				Event::fire("alert.any", [$this->alerts]);
				Event::fire("alert.{$prefix}", [$this->alerts]);

			}

		}

	}

	public function getIcon($icon=NULL) {

		if(array_key_exists($icon, $this->icons)) {

			return $this->icons[$icon];

		}

		return NULL;
	}

	public function fire($path='alert::simple') {

		if(empty($path) OR empty($this->alerts)) return NULL;

		if(view()->exists($path)) {

			return view($path, $this->alerts);

		}

		return NULL;

	}

}

/* End of Controller PrefixAlert.php */