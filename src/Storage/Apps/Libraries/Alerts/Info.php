<?php namespace Library\Alerts;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

class Info {

	public function apps() {

		return [

			"type"			=> "library",
			"slug"			=> "alert",
			"provider"		=> \Library\Alerts\AlertServiceProvider::class,
			"facade"		=> \Library\Alerts\Support\Alert::class,
			"token"			=> NULL,
			"activated" 	=> 1,

		];

	}

	public function meta() {

		return [
			
			"name"			=> "Alert",
			"author"		=> "Ing. Ramón A Linares Febles",
			"email"			=> "rlinares4381@gmail.com",
			"license"		=> "MIT",
			"site"			=> "http://www.iipec.net",
			"version"		=> "V-1.0",
			"description" 	=> "Librería de alertas.",

		];

	}

	public function handler($apps) {
		
		$apps->create($this->apps())->addMeta($this->meta());

	}

}

/* End of Info.php */