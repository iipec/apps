
	<article class="alert alert-{{$state}}">

		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>

		{!!$icon!!} <strong>{!!$title!!}! </strong> {!!$message!!}
		
	</article>