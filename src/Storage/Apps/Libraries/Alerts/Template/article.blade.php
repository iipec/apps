
	<article class="alert alert-{{$state}} alert-article">		

		<header class="alert-header">

			{!!$icon!!} {!!$title!!}!

			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<i class="mdi mdi-close-outline"></i>
			</button>
			
		</header>

		<section class="alert-body">

			@if(is_array($message))

			@foreach($message as $row)
			<p>{!!$row!!}</p>
			@endforeach

			@else
			<p>{!!$message!!}</p>
			@endif
			
		</section>
		
	</article>