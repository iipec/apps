<?php namespace Library\Users;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Library\Users\Support\User;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider {

	public function boot() {
		
	}

	public function register() {

		$this->app->bind("User", function($app) {
			return new \Library\Users\Support\UserSupport($app);
		});

		User::singleton("userdb", new \Library\Users\Models\Account\UserModel);
		User::singleton("infodb", new \Library\Users\Models\Account\UserInfoModel);
		User::singleton("metadb", new \Library\Users\Models\Account\UserMetaModel);
		User::singleton("resetdb", new \Library\Users\Models\Account\UserResetModel);
		User::singleton("sessiondb", new \Library\Users\Models\Account\UserSessionModel);
		User::singleton("roldb", new \Library\Users\Models\Rol\UserRolModel);
		User::singleton("groupdb", new \Library\Users\Models\Group\UserGroupModel);

		require_once(__DIR__."/Support/UserCommon.php");

	}

}


/* End of providers UserServiceProvider.php */