<?php namespace Library\Users\Models\Group;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;
use Library\Users\Models\Rol\UserRolModel;
use Library\Users\Models\Account\UserModel;
use Library\Users\Models\Group\Traits\UserGroupTrait;

class UserGroupModel extends Model {

	use UserGroupTrait;
	
	protected $table = "groups";

	protected $fillable = [
		"parent", "slug", "description", "counts", "activated"
	];

	public $timestamps = false;

	public function setParentAttribute($value) {

		if( !is_numeric($value) && is_numeric( ($ID = $this->src($value)->id) ) ) {
			return $this->attributes['parent'] = $ID;
		}

        return $this->attributes['parent'] = $value;
    }

    public function users() {
    	return $this->belongsToMany(UserModel::class, "groups_users", "user_id", "group_id");
    }

    public function rols() {
    	return $this->belongsToMany(UserRolModel::class, "groups_rols", "group_id", "rol_id");
    }

}

/* End of Model UserGroupModel.php */