<?php namespace Library\Users\Models\Group\Traits;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

trait UserGroupTrait {
	
	public function ID($slug=NULL) {
		return $this->src($slug)->id;
	}
	
	public function src($slug=NULL) {

		if(empty($slug)) return NULL;

		if( ($query = $this->where("id", $slug)->orWhere("slug", $slug))->count() > 0 ) {

			return $query->first();

		}

		return NULL;
	}

	public function add($data=NULL, $rols=NULL) {

		if(!empty($data) && is_array($data)) {

			$toggles = NULL;

			foreach ($data as $row) {
				
				if(empty($rols)) {
					$this->create($row);
				}
				else {

					$this->create($row)->syncRol($rols);

				}

			}


		}

		return $this;
	}

	public function syncRol($data=NULL) {

    	if(!empty($data) && is_array($data) && !empty( ($rols =rolsID($data)) ) ) {

    		$this->rols()->sync($rols);
    		
    	}

    	return $this;

    }

}

/* End of Library UserGroupTrait.php */