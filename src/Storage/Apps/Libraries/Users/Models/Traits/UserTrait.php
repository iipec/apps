<?php namespace Library\Users\Models\Traits;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

trait UserTrait {

	public function src($user=NULL) {

		if(empty($user)) return NULL;

		if( ($query = $this->where("id", $user)->orWhere("user", $user)->orWhere("email", $user))->count() > 0 ) {

			return $this->first();

		}

		return NULL;
	}
	
	public function addInfo($data=[]) {

		if(!empty($data) && is_array($data)) {

			$this->info()->create($data);

		}

		return $this;
		
	}

	public function addMeta($data=[]) {

		if(!empty($data) && is_array($data)) {

			foreach ($data as $key => $value) {

				$this->meta()->create(["key" => $key, "value" => $value]);

			}

		}

		return $this;
		
	}

	public function syncGroup($groups=NULL) {

		if(!empty($groups)) {

			$this->groups()->sync(groupsID($groups));

		}

		return $this;
		
	}

}

/* End of Library UserTrait.php */