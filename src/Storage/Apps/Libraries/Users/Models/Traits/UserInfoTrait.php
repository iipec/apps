<?php namespace Library\Users\Models\Traits;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

trait UserInfoTrait {

	public function scopeSearch($query, $UI) {

		return $query->where("user_id", $UI)
					 ->orWhere("dni", $UI)
					 ->orWhere("cellphone", $UI)
					 ->orWhere("ofice_phone");
					 
	}
	
	public function src($UI=NULL) {

		if(empty($UI)) return NULL;

		if( ($query = $this->search($UI))->count() > 0 ) {

			return $this->first();

		}

		return NULL;
	}

}

/* End of Library UserInfoTrait.php */