<?php namespace Library\Users\Models\Rol\Traits;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

trait UserRolTrait {
	
	public function src($rol=NULL) {

		if(empty($rol)) return NULL;

		if( ($query = $this->where("id", $rol)->orWhere("rol", $rol))->count() > 0 ) {

			return $query->first();

		}

		return NULL;
	}

	public function ID($rol=NULL) {

		if( ($query = $this->where("rol", $rol))->count() > 0 ) {

			return $query->first()->id;

		}

		return NULL;

	}

	public function add($data=NULL) {

		if( !empty($data) && is_array($data) ) {

			foreach ($data as $row) {
				
				$this->create($row);

			}

		}

		return $this;

	}

}

/* End of Library UserRolTrait.php */