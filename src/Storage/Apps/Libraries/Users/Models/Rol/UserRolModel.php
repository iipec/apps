<?php namespace Library\Users\Models\Rol;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;
use Library\Users\Models\Group\UserGroupModel;
use Library\Users\Models\Rol\Traits\UserRolTrait;

class UserRolModel extends Model {
	
	use UserRolTrait;

	protected $table = "rols";

	protected $fillable = [
		"rol", "description"
	];

	public $timestamps = false;

	public function groups() {
    	return $this->belongsToMany(UserGroupModel::class, "groups_rols", "rol_id", "group_id");
    }

}

/* End of Model UserRolModel.php */