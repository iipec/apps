<?php namespace Library\Users\Models\Account;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;

class UserSessionModel extends Model {
	
	protected $table = "users_session";

	protected $fillable = [
		"user_id",
		"latitude",
		"longitude", 
		"ip_address", 
		"agent", 
		"last_activity",
		"created_at",
		"updated_at"
	];

}

/* End of Model UserSessionModel.php */