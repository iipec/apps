<?php namespace Library\Users\Models\Account;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;

class UserMetaModel extends Model {
	
	protected $table = "users_meta";

	protected $fillable = [
		"key", "value"
	];

	public $timestamps = false;

}

/* End of Model UserMetaModel.php */