<?php namespace Library\Users\Models\Account;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;

class UserInfoModel extends Model {
	
	protected $table = "users_info";

	protected $fillable = [

		"firstname",
		"lastname",
		"birthday",
		"place_birth",
		"nationality",
		"dni",
		"current_address",
		"street",
		"no",
		"sector",
		"home_phone",
		"cellphone",
		"office_phone",
		"blood_type",
		"allergic_to",
		"medical_note",
		"civil_status",
		"ocupation",
		"work_place",
		"created_at",
		"updated_at"

	];

	public function scopeSearch($query, $UI) {

		return $query->where("user_id", $UI)
					 ->orWhere("dni", $UI)
					 ->orWhere("cellphone", $UI)
					 ->orWhere("ofice_phone");
					 
	}
	
	public function src($UI=NULL) {

		if(empty($UI)) return NULL;

		if( ($query = $this->search($UI))->count() > 0 ) {

			return $this->first();

		}

		return NULL;
	}

}

/* End of Model UserInfoModel.php */