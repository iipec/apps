<?php namespace Library\Users\Models\Account;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Database\Eloquent\Model;

class UserResetModel extends Model {
	
	protected $table = "users_reset";

	protected $fillable = [
		"email", "token", "created_at"
	];

}

/* End of Model UserResetModel.php */