<?php namespace Library\Users\Models\Account;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Notifications\Notifiable;
use Library\Users\Models\Group\UserGroupModel;
use Library\Users\Models\Account\UserInfoModel;
use Library\Users\Models\Account\UserMetaModel;
use Library\Users\Models\Account\UserSessionModel;
use Library\Users\Models\Account\Traits\UserTrait;

use Illuminate\Foundation\Auth\User;

class UserModel extends User {

    use Notifiable, UserTrait;
	
	protected $table = "users";

	protected $fillable = [
		
		"id", 
		"public_name", 
		"user", 
		"email", 
		"password", 
		"avatar",
		"type", 
		"activated", 
		"created_at", 
		"updated_at"

	]; 

    protected $hidden = [
        'password', 'remember_token',
    ];

	public function setPasswordAttribute($value) {

        $value = trim($value);
        $value = bcrypt($value);

        $this->attributes['password'] = $value;
        
    }

    public function info() {
    	return $this->hasOne(UserInfoModel::class, "user_id");
    } 

    public function meta() {
    	return $this->hasMany(UserMetaModel::class, "user_id");
    }

    public function groups() {
    	return $this->belongsToMany(UserGroupModel::class, "groups_users", "user_id", "group_id");
    }

    public function rols() {
    }

    public function session() {
    	return $this->hasMany(UserSessionModel::class, "user_id");
    }
}

/* End of Model UserModel.php */