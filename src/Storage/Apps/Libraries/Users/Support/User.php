<?php namespace Library\Users\Support;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Facade;

class User extends Facade {

	public static function getFacadeAccessor() {

		return "User";
		
	}
	
}

/* End of Facade User.php */