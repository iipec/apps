<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

if(!function_exists("user")) {

	function user($key=NULL) {

		return \Library\Users\Support\User::app($key);

	}
	
}

if(!function_exists("groupID")) {

	function groupID($group=NULL) {

		return user("groupdb")->ID($group);

	}

}

if(!function_exists("groupsID")) {

	function groupsID($groups=NULL) {

		if(!empty($groups)) {

			if(!is_array($groups)) $groups = [$groups];

			$IDS = NULL;

			foreach ($groups as $group) {
				$IDS[] = groupID($group);
			}

			return $IDS;
		}

		return NULL;
	}

}

if(!function_exists("rolID")) {

	function rolID($rol=NULL) {

		return user("roldb")->ID($rol);

	}
}

if(!function_exists("rolsID")) {

	function rolsID($rols=NULL) {

		if(!empty($rols)) {

			if(!is_array($rols)) $rols = [$rols];
			
			$IDS = NULL;

			foreach ($rols as $rol) {
				
				$IDS[] = rolID($rol);

			}

			return $IDS;
		}

		return NULL;
	}

}

/* End of helper UserCommon.php */