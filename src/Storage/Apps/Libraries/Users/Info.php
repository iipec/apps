<?php namespace Library\Users;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

class Info {

	public function apps() {

		return [

			"type"			=> "library",
			"slug"			=> "user",
			"provider"		=> \Library\Users\UserServiceProvider::class,
			"facade"		=> \Library\Users\User::class,
			"token"			=> NULL,
			"activated" 	=> 1,

		];

	}

	public function meta() {

		return [

			"name"			=> "Users",
			"author"		=> "Ing. Ramón A Linares Febles",
			"email"			=> "rlinares4381@gmail.com",
			"license"		=> "MIT",
			"site"			=> "http://www.iipec.net",
			"version"		=> "V-1.0",
			"description" 	=> "Librería de usuarios.",

		];

	}

	public function handler($apps) {
		
		$apps->create($this->apps())->addMeta($this->meta());

	}

}

/* End of Info.php */