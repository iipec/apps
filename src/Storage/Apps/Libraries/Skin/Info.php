<?php namespace Library\Skin;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

class Info {

	public function apps() {

		return [

			"type"			=> "library",
			"slug"			=> "skin",
			"provider"		=> \Library\Skin\SkinServiceProvider::class,
			"facade"		=> \Library\Skin\Skin::class,
			"token"			=> NULL,
			"activated" 	=> 1,

		];

	}

	public function meta() {

		return [

			"name"			=> "Skin",
			"author"		=> "Ing. Ramón A Linares Febles",
			"email"			=> "rlinares4381@gmail.com",
			"license"		=> "MIT",
			"site"			=> "http://www.iipec.net",
			"version"		=> "V-1.0",
			"description" 	=> "Librería de gestión de las plantillas.",

		];

	}

	public function handler($apps) {
		
		$apps->create($this->apps())->addMeta($this->meta());

	}

}

/* End of Info.php */