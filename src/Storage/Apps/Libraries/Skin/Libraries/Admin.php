<?php namespace Library\Skin\Libraries;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


class Admin {
	
	protected $skin;

	public function __construct($skin) {
		
		$this->skin = $skin;

	}

	public function name() {

		return $this->skin;
		
	}

	public function path($path=NULL) {
		
		//return $this->skin."::".$path;

	}

	public function render($view=NULL, $data=[], $mergeData=[]) {
		
		if(view()->exists( ($view = $this->path($view)) )) {

			return view($view, $data, $mergeData);

		}

		abort(404, "View {$view} no exist");
		
	}

}

/* End of Controller Admin.php */