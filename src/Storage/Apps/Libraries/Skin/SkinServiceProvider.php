<?php namespace Library\Skin;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Library\Skin\Skin;
use Illuminate\Support\ServiceProvider;

class SkinServiceProvider extends ServiceProvider {

	public function boot() {

		require_once(__DIR__."/SkinCommon.php");

	}

	public function register() {

		$this->app->bind("Skin", function($app) {

			return new \Library\Skin\SkinSupport($app);

		});

		Skin::load("admin", new \Library\Skin\Libraries\Admin(config("admin.skin")));

	}

}


/* End of providers SkinServiceProvider.php */