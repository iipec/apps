<?php namespace Library\Skin;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/


use Apps\Accessors\SingletonAccessor;

class SkinSupport extends SingletonAccessor {

	protected $skin;
	
	public function __construct($app) {
		
	}

	public function admin($path=NULL) {

		if( ($skin = config("admin.skin", "apps")) != "apps") {

			return $skin."::".$path;

		}

		abort(404, "No se ha definido una plantilla");
	}

	public function web($path=NULL) {

	}

}

/* End of Controller SkinSupport.php */