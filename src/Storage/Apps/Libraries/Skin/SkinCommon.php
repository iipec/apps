<?php

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

if(!function_exists("skin")) {

	function skin($key=NULL) {

		return Library\Skin\Skin::app($key);

	}

}

if(!function_exists("is_admin_skin")) {

	function is_admin_skin($slug=NULL) {

		if(!empty($slug)) {

			return (config("admin.skin") == $slug);
		}

		return FALSE;
	}
}

/* End of helper SkinCommon.php */