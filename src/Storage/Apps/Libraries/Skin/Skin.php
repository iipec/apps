<?php namespace library\Skin;

/*
 *---------------------------------------------------------
 * ©IIPEC
 * Santo Domingo República Dominicana.
 *---------------------------------------------------------
*/

use Illuminate\Support\Facades\Facade;

class Skin extends Facade {

	public static function getFacadeAccessor() {

		return "Skin";
		
	}
	
}

/* End of Facade Skin.php */